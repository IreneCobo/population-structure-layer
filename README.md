# Population Structure layer

## 1. fastSTRUCTURE

The following steps have the objective of transforming the vcf files containing the SNPs (available in TPPS) into a .bed, .fam and .bim files, required as inputs to run fastSTRUCTURE program. To this end, this workflow makes use of *plink* (fastSTRUCTURE Step 1):

```
plink --vcf Panel1imputedhweLD0.2.vcf --allow-extra-chr --double-id --make-bed --out Panel1imputedhweLD0.2
```

- Run fastSTRUCTURE

```
structure.py -K 2 --input=Panel1imputedhweLD0.2 --output=Panel1imputedhweLD0.2 --full
```

In the above command, -K 2 specifies the number of ancestral populations that fastStructure should create. Both panels (1 and 2) showed 2 populations in the PCA.

This code produces five output files:

- Panel1imputedLD0.2.2.log
- Panel1imputedLD0.2.2.meanP
- Panel1imputedLD0.2.2.meanQ
- Panel1imputedLD0.2.2.varP
- Panel1imputedLD0.2.2.varQ

We are going to use the *meanQ* file to perform the population structure correction during the GWAS.This *meanQ* file contains 1 row for each sample, and 1 column for each ancestral population. The numbers give the proportion of the genome inferred to have come from the ancestral population.

The format has to be changed to accomplish the requisites of the program we are going to use to perform GWAS: EMMAX. The population structure file has to have three entries at each line, FAMID, INDID, intercept (so the third column is recommended to be 1 always) and the population to which each individual belongs.

**1.** Extract the ID of the individuals from the .fam file (obtained from running "fastSTRUCTURE Step 1").

```
awk 'BEGIN { OFS = "_" } ;{print $1,$2}' Panel2imputedLD0.2.fam > IDPanel2.txt
sed 's/_/\t/g' IDPanel2.txt > IDPanel2tab.txt
awk '{print $1,$2}' IDPanel2tab.txt > IDfamPanel2.txt
```


Now we will use these proportions to assign each individual to a particular population. To this end, CartograPlant will use R. The R code below assigns an individual to a population based on whichever ancestral genome has the highest proportion. This assignment can be problematic for heavily admixed individuals.

```
fs <-read.table("Panel1imputedLD0.2.2.meanQ", header=F)
fs$assignedPop <- apply(fs, 1, which.max)
```
Create the intercept column and add it to the table :

```
intercept<-rep(1,407)#the second number correspond with the number of individuals in each dataset, in the case of panel 1, 407 individuals
fsintercept<-cbind(intercept,fs)
```

Read the ID names and add the individual IDs to the table

```
ID<-read.table("IDfamPanel1.txt")
fsID<-cbind(ID,fsintercept)
```
Retain only the first column (Individual IDs) and the last column (population assignment):

```
fsPop<-fsID[,c(1,2,3,6)] # the forth number is the number of ancestral populations created by fastSTRUCTURE + 4
```

Write a txt table, space delimited, to be included as covariate in EMMAX GWAS analysis:

```
write.table(fsPop,"assignedPopPanel1.txt", col.names=F,quote=F,sep=" ")
```
Here finishes the R code

A final step is to remove the first column (bug from the R code):

```
cut -d\  -f2- assignedPopPanel1.txt > assignedPopPanel1final.txt
```

Make sure that the population structure txt file has the following format (famID, indID, intercept, population):

```
206 206 1 3
207 207 1 3
208 208 1 3
209 209 1 1
210 210 1 1
211 211 1 3
212 212 1 3
213 213 1 3
```

## 2. Plot the population structure information

Now, we will plot this population structure information using again R. This is only to check how the points will look like in a map and verify that everything looks correct

```
location<-read.csv("TGDR372_Plant_Accession_Populus_trichocarpa_0.csv")## Accession information with latitude and longitude, available in TPPS
location2<-location[,c(1,4,5)]#get the PlantID, latitude and longitude columns

population<-read.table("assignedPopPanel1final.txt")#Population infomration obtained in the previous step
population2<-population[,c(2,4)]#get the second (plant identifier) and forth (population number) columns
colnames(population2)<-c("Plant.Identifier","Population")#add colnames to the population file

locpop<-merge(population2,location2)#merge the two files containing the locations and the population information

locpop$Population<-as.character(locpop$Population)#transform the population numbers into characters to be plotted in the map and to assign them different colors

#install.packages("raster")
library(raster)
#install.packages("tidyverse")
library(tidyverse)
#install.packages("mapdata")
library(mapdata)
library(ggplot2)
library(maps)
#install.packages("ggrepel")
library(ggrepel)
#install.packages("ggthemes")
library(ggthemes)
#install.packages("ggspatial")
library(ggspatial)

#Plot the map in R

mapa_mundo <- map_data("world")

dem.raster <- getData("SRTM", lat = 50.71, lon = -130.97, download = TRUE)
dem.m  <-  rasterToPoints(dem.raster)
dem.df <-  data.frame(dem.m)
colnames(dem.df) = c("lon", "lat", "alt")

options(scipen = 999) # to avoid scientific notation 

mapa_mundo %>%
  ggplot() +
  geom_tile(data = dem.df, aes(lon, lat, fill = alt), alpha = .80) +
  scale_fill_gradientn(colours = terrain.colors(200)) +
  geom_polygon(aes( x= long, y = lat, group = group),
               fill = NA,
               color = "black") +
  geom_point(data= locpop, 
             aes(x= Longitude, y = Latitude, color = Population), 
             stroke = F) +
  coord_fixed(xlim= c(-153.61, -118.01),
              ylim= c(37.43,62.03),
              ratio= 1.1)+
  scale_color_manual(values = c( "green", "blue", "red"), name = " ") + 
  scale_shape_discrete(solid=T)+
  annotation_scale() +
  annotation_north_arrow(location='tr')

theme_map()
```

![PopStructure](Panel1PopulationStructuremap.png "PopStructure")


Now we are going to transform the points into a shapefile using the following code in R

````
#install.packages("shapefiles")
library(shapefiles)
#install.packages("PBSmapping")
library(PBSmapping)
#install.packages("adehabitatHR")
library(adehabitatHR)
#install.packages("maptools")
library(maptools)
library(maps)
library(rgdal)
#install.packages("igraph")
library(igraph)

locpop2<-locpop[,c(3,4)] #extract the location of the points (latitude and longitude)
locpopPoints<-SpatialPoints(locpop2) #convert locpop2 to spatial points
pop<-locpop[,c(1,2)]#include the plant identifier and the population as data in the shapefile

obj<-SpatialPointsDataFrame(coords=locpop2,data=pop) 
writePointsShape(obj,"Panel1PopulationStructure.shp")
```
